package br.com.empresasioasys.responses;


import br.com.empresasioasys.models.Investor;

public class LoginResponse {


    public Investor investor;

    @Override
    public String toString() {
        return "LoginResponse{" +
                "investor=" + investor +
                '}';
    }
}
