package br.com.empresasioasys.responses;

import java.util.List;

import br.com.empresasioasys.models.Enterprise;


public class EnterpriseListResponse {

    public List<Enterprise> enterprises;
}
