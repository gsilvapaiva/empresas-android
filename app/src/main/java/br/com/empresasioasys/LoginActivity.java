package br.com.empresasioasys;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import br.com.empresasioasys.auth.Auth;
import br.com.empresasioasys.callbacks.LoginCallback;
import br.com.empresasioasys.config.ConnectionConfig;
import br.com.empresasioasys.models.User;
import br.com.empresasioasys.responses.LoginResponse;
import br.com.empresasioasys.services.UserService;
import br.com.empresasioasys.utils.ValidationUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LoginActivity extends AppCompatActivity {


    private EditText loginEmail;
    private EditText loginPassword;
    private Button loginBtn;
    private ProgressDialog progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        instantiateViewObjects();

        Retrofit retrofit = new Retrofit.Builder().
                baseUrl(ConnectionConfig.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        final UserService userService = retrofit.create(UserService.class);



        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Validation of input fields
                if(ValidationUtil.validLoginInput(loginEmail,loginPassword,getBaseContext()))
                {
                    User user = new User(loginEmail.getText().toString(),loginPassword.getText().toString());

                    progressDialog = new ProgressDialog(LoginActivity.this);
                    progressDialog.setMessage("Realizando login");
                    progressDialog.show();

                    Call<LoginResponse> loginUser = userService.loginUser(user);

                    loginUser.enqueue(new LoginCallback(LoginActivity.this,progressDialog));
                }


            }
        });






    }

    private void instantiateViewObjects(){
        this.loginEmail = (EditText)findViewById(R.id.loginEmail);
        this.loginPassword = (EditText)findViewById(R.id.loginPassword);
        this.loginBtn = (Button)findViewById(R.id.loginBtn);

    }
}
