package br.com.empresasioasys;

import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import org.w3c.dom.Text;

import br.com.empresasioasys.models.Enterprise;

public class EnterpriseInfoActivity extends AppCompatActivity {


    private ImageView enterpriseImg;
    private TextView enterpriseDescription;
    private TextView enterpriseName;
    private ImageView iconBack;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enterprise_info);
        instantiateViews();

        Enterprise enterprise = (Enterprise) getIntent().getSerializableExtra("Enterprise");
        this.enterpriseDescription.setText(enterprise.getDescription());
        this.enterpriseName.setText(enterprise.getEnterprise_name());

        if(enterprise.getPhoto() == null){
            this.enterpriseImg.setImageDrawable(ContextCompat.getDrawable(this,R.drawable.img_e_1));
        }
        else{
            Glide.with(this).load(enterprise.getPhoto()).into(enterpriseImg);
        }

        //Finish the activity to back to previous activity on android callstack
        iconBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    private void instantiateViews(){
        this.enterpriseImg = (ImageView)findViewById(R.id.enterprise_img);
        this.enterpriseDescription = (TextView)findViewById(R.id.enterprise_description);
        this.enterpriseName = (TextView)findViewById(R.id.toolbar_enterprise_name);
        this.iconBack = (ImageView)findViewById(R.id.icon_back);
    }


}
