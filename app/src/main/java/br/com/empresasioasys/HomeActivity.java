package br.com.empresasioasys;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.io.IOException;

import br.com.empresasioasys.adapters.EnterprisesAdapter;
import br.com.empresasioasys.auth.Auth;
import br.com.empresasioasys.callbacks.EnterpriseListResponseCallback;
import br.com.empresasioasys.config.ConnectionConfig;
import br.com.empresasioasys.interfaces.OnItemClickListener;
import br.com.empresasioasys.listeners.EditTextChanged;
import br.com.empresasioasys.listeners.ItemRecyclerViewClicked;
import br.com.empresasioasys.responses.EnterpriseListResponse;
import br.com.empresasioasys.services.EnterpriseService;
import br.com.empresasioasys.services.UserService;
import br.com.empresasioasys.utils.ConstantsUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class HomeActivity extends AppCompatActivity  {


    private ImageView searchBtn;
    private LinearLayout searchFields;
    private ImageView logoHome;
    private ImageView closeBtn;
    private TextView textSearchHome;
    private RecyclerView mRecyclerView;
    private EnterprisesAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private ProgressDialog progressDialog;
    private EditText edtQuery;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        instantiateViews();

        Retrofit retrofit = new Retrofit.Builder().
                baseUrl(ConnectionConfig.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        final EnterpriseService enterpriseService = retrofit.create(EnterpriseService.class);

        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        final Call<EnterpriseListResponse> enterpriseList = enterpriseService.enterprisesList(
                Auth.getAccessToken(this),Auth.getClient(this),Auth.getUid(this),
                ConstantsUtil.ENTERPRISE_TYPE,null);


        progressDialog = new ProgressDialog(HomeActivity.this);

        edtQuery.addTextChangedListener(new EditTextChanged(enterpriseService,this,mAdapter,mRecyclerView));


        searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchFields.setVisibility(View.VISIBLE);
                searchBtn.setVisibility(View.GONE);
                logoHome.setVisibility(View.GONE);
                textSearchHome.setVisibility(View.GONE);
                progressDialog.setMessage("Recuperando informações.");
                progressDialog.show();
                enterpriseList.clone().enqueue(new EnterpriseListResponseCallback(enterpriseService,HomeActivity.this,mAdapter,mRecyclerView,progressDialog));

            }
        });
        closeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchFields.setVisibility(View.GONE);
                searchBtn.setVisibility(View.VISIBLE);
                logoHome.setVisibility(View.VISIBLE);
                textSearchHome.setVisibility(View.VISIBLE);
                mRecyclerView.setVisibility(View.GONE);
                edtQuery.setText("");

            }
        });

    }

    private void instantiateViews(){

        this.searchBtn = (ImageView)findViewById(R.id.searchBtn);
        this.searchFields = (LinearLayout)findViewById(R.id.searchFieldsLayout);
        this.logoHome = (ImageView) findViewById(R.id.logoHome);
        this.closeBtn = (ImageView) findViewById(R.id.closeBtn);
        this.textSearchHome = (TextView) findViewById(R.id.textSearchHome);
        this.mRecyclerView = (RecyclerView) findViewById(R.id.enterprisesList);
        this.edtQuery = (EditText)findViewById(R.id.edtQuery);
    }


}
