package br.com.empresasioasys.models;


public class Investor {

    private long id;
    private String investor_name;
    private String email;
    private String city;
    private String country;
    private int balance;
    private String photo;
    private Portfolio portfolio;
    private int portfolio_value;
    private boolean first_access;
    private boolean super_angel;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getInvestor_name() {
        return investor_name;
    }

    public void setInvestor_name(String investor_name) {
        this.investor_name = investor_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public Portfolio getPortifolio() {
        return portfolio;
    }

    public void setPortifolio(Portfolio portfolio) {
        this.portfolio = portfolio;
    }

    public int getPortifolio_value() {
        return portfolio_value;
    }

    public void setPortifolio_value(int portifolio_value) {
        this.portfolio_value = portifolio_value;
    }

    public boolean isFirst_access() {
        return first_access;
    }

    public void setFirst_access(boolean first_access) {
        this.first_access = first_access;
    }

    public boolean isSuper_angel() {
        return super_angel;
    }

    public void setSuper_angel(boolean super_angel) {
        this.super_angel = super_angel;
    }

    @Override
    public String toString() {
        return "Investor{" +
                "id=" + id +
                ", investor_name='" + investor_name + '\'' +
                ", email='" + email + '\'' +
                ", city='" + city + '\'' +
                ", country='" + country + '\'' +
                ", balance=" + balance +
                ", photo='" + photo + '\'' +
                ", portifolio=" + portfolio +
                ", portifolio_value=" + portfolio_value +
                ", first_access=" + first_access +
                ", super_angel=" + super_angel +
                '}';
    }
}
