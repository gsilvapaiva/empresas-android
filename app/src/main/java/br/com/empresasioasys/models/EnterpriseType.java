package br.com.empresasioasys.models;

import java.io.Serializable;

/**
 * Created by gabriel on 01/08/17.
 */

public class EnterpriseType implements Serializable{

    private int id;
    private String enterprise_type_name;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEnterprise_type_name() {
        return enterprise_type_name;
    }

    public void setEnterprise_type_name(String enterprise_type_name) {
        this.enterprise_type_name = enterprise_type_name;
    }

    @Override
    public String toString() {
        return "EnterpriseType{" +
                "id=" + id +
                ", enterprise_type_agro='" + enterprise_type_name + '\'' +
                '}';
    }
}
