package br.com.empresasioasys.utils;


import android.content.Context;
import android.widget.EditText;
import android.widget.Toast;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class ValidationUtil {


    public static boolean verifyEmptyFields(String email, String password){

        if(email.equals("") |  password.equals("")){
            return false;
        }
        return true;
    }
    public static boolean verifyEmail(String email){
        Pattern regEx = Pattern.compile(".+@.+\\.[a-z]+");
        Matcher matcher = regEx.matcher(email);

        if(!matcher.matches()){
            return false;
        }

        return true;
    }
    public static boolean verifyPassword(String password){
        if(password.length() < 6)
            return false;

        return true;
    }

    public static boolean validLoginInput(EditText email, EditText password, Context context){

        if(!ValidationUtil.verifyEmptyFields(email.getText().toString(),password.getText().toString())){
            Toast.makeText(context,"Os campos email e senha são obrigatórios.",Toast.LENGTH_LONG).show();
            return false;
        }
        if(!ValidationUtil.verifyEmail(email.getText().toString())){
            email.setError("Email inválido, (example@example.com é valido).");
            Toast.makeText(context,"Email inválido, (example@example.com é valido).",Toast.LENGTH_LONG).show();
            return false;
        }

        if(!ValidationUtil.verifyPassword(password.getText().toString())){
            password.setError("Sua senha deve possuir no mínimo 6 caracteres.");
            return false;
        }

        return true;

    }





}
