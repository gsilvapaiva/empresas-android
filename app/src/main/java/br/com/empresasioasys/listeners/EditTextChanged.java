package br.com.empresasioasys.listeners;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import br.com.empresasioasys.adapters.EnterprisesAdapter;
import br.com.empresasioasys.auth.Auth;
import br.com.empresasioasys.callbacks.EnterpriseListQueryResponseCallback;
import br.com.empresasioasys.responses.EnterpriseListResponse;
import br.com.empresasioasys.services.EnterpriseService;
import retrofit2.Call;


public class EditTextChanged implements TextWatcher {

    EnterpriseService enterpriseService;
    Context context;
    EnterprisesAdapter mAdapter;
    RecyclerView mRecyclerView;

    public EditTextChanged(EnterpriseService enterpriseService, Context context,EnterprisesAdapter mAdapter,RecyclerView mRecyclerView){
        this.enterpriseService = enterpriseService;
        this.context = context;
        this.mAdapter = mAdapter;
        this.mRecyclerView = mRecyclerView;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {

        Call<EnterpriseListResponse> enterpriseList = enterpriseService.enterprisesList(
                Auth.getAccessToken(context),Auth.getClient(context),Auth.getUid(context),
                1,s.toString());

        enterpriseList.enqueue(new EnterpriseListQueryResponseCallback(enterpriseService,context,mAdapter,mRecyclerView));

    }
}
