package br.com.empresasioasys.auth;


import android.content.Context;
import android.content.SharedPreferences;

public class Auth {

    private static String client;
    private static String accessToken;
    private static String uid;
    private static final String PREF_CLIENT = "CLIENT";
    private static final String PREF_UID = "UID";
    private static final String PREF_ACCESSTOKEN = "ACCESS-TOKEN";

    public static String getAccessToken(Context context) {
        SharedPreferences settings = context.getSharedPreferences(PREF_ACCESSTOKEN, context.MODE_PRIVATE);
        accessToken = settings.getString("access-token", null);
        return accessToken;
    }

    public static void saveAccessToken(Context context, String accessToken) {
        SharedPreferences.Editor editor = context.getSharedPreferences(PREF_ACCESSTOKEN, context.MODE_PRIVATE).edit();
        editor.putString("access-token",accessToken );
        editor.commit();
    }

    public static String getClient(Context context) {
        SharedPreferences settings = context.getSharedPreferences(PREF_CLIENT, context.MODE_PRIVATE);
        client = settings.getString("client", null);
        return client;
    }

    public static void saveClient(Context context, String client) {
        SharedPreferences.Editor editor = context.getSharedPreferences(PREF_CLIENT, context.MODE_PRIVATE).edit();
        editor.putString("client",client );
        editor.commit();
    }
    public static String getUid(Context context) {
        SharedPreferences settings = context.getSharedPreferences(PREF_UID, context.MODE_PRIVATE);
        uid = settings.getString("uid", null);
        return uid;
    }

    public static void saveUid(Context context, String uid) {
        SharedPreferences.Editor editor = context.getSharedPreferences(PREF_UID, context.MODE_PRIVATE).edit();
        editor.putString("uid",uid );
        editor.commit();
    }


}
