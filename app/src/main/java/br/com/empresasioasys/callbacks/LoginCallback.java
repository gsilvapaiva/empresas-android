package br.com.empresasioasys.callbacks;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import br.com.empresasioasys.HomeActivity;
import br.com.empresasioasys.LoginActivity;
import br.com.empresasioasys.auth.Auth;
import br.com.empresasioasys.responses.LoginResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginCallback implements Callback<LoginResponse> {

    Context context;
    ProgressDialog progressDialog;

    public LoginCallback(Context c , ProgressDialog p){
        this.context = c;
        this.progressDialog = p;
    }



    @Override
    public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
        progressDialog.dismiss();
        if(response.isSuccessful()){
            LoginResponse loginResponse =  response.body();

            Auth.saveAccessToken(context,response.headers().get("access-token"));
            Auth.saveClient(context,response.headers().get("client"));
            Auth.saveUid(context,response.headers().get("uid"));

            //redirect to another activity
            Intent i = new Intent(context,HomeActivity.class);
            context.startActivity(i);

        }
        else{
            if(response.code() == 401){
                Toast.makeText(context,"Email ou senha inválidos.",Toast.LENGTH_LONG).show();
            }
        }

    }

    @Override
    public void onFailure(Call<LoginResponse> call, Throwable t) {
        progressDialog.dismiss();
        Toast.makeText(context,"Algo deu errado, verifique sua conexão e tente novamente.",Toast.LENGTH_LONG).show();
    }



}
