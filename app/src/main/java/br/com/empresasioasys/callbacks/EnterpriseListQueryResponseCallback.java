package br.com.empresasioasys.callbacks;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import br.com.empresasioasys.adapters.EnterprisesAdapter;
import br.com.empresasioasys.interfaces.OnItemClickListener;
import br.com.empresasioasys.responses.EnterpriseListResponse;
import br.com.empresasioasys.services.EnterpriseService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by gabriel on 06/08/17.
 */

public class EnterpriseListQueryResponseCallback implements Callback<EnterpriseListResponse> {

    EnterpriseService enterpriseService;
    Context context;
    EnterprisesAdapter mAdapter;
    RecyclerView mRecyclerView;

    public EnterpriseListQueryResponseCallback(EnterpriseService enterpriseService, Context context, EnterprisesAdapter mAdapter, RecyclerView mRecyclerView) {
        this.enterpriseService = enterpriseService;
        this.context = context;
        this.mAdapter = mAdapter;
        this.mRecyclerView = mRecyclerView;
    }

    @Override
    public void onResponse(Call<EnterpriseListResponse> call, Response<EnterpriseListResponse> response) {

        if(response.body() == null){
            mAdapter = new EnterprisesAdapter(null,context);
        }
        else{
            mAdapter = new EnterprisesAdapter(response.body().enterprises,context);
        }
        mAdapter.OnItemClickListener(new OnItemClickListener() {
            @Override
            public void OnItemClickListener(View v, int position) {
                mAdapter.openEnterpriseInfoActivity(position);
            }
        });

        mRecyclerView.setAdapter(mAdapter);


    }

    @Override
    public void onFailure(Call<EnterpriseListResponse> call, Throwable t) {

    }
}
