package br.com.empresasioasys.adapters;


import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.List;

import br.com.empresasioasys.EnterpriseInfoActivity;
import br.com.empresasioasys.R;
import br.com.empresasioasys.interfaces.OnItemClickListener;
import br.com.empresasioasys.models.Enterprise;

import static android.R.attr.start;

public class EnterprisesAdapter extends RecyclerView.Adapter<EnterprisesAdapter.EnterprisesHolder>  {


    private List<Enterprise> enterprises;
    private Context context;
    private LayoutInflater l;

    private static OnItemClickListener onItemClickListener;


    public EnterprisesAdapter(List<Enterprise> enterprises, Context c){

        this.enterprises = enterprises;
        this.context = c;
        this.l = (LayoutInflater)c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public EnterprisesHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = this.l.inflate(R.layout.card_enterprise,parent,false);
        EnterprisesHolder enterprisesHolder = new EnterprisesHolder(v);
        return enterprisesHolder;
    }

    @Override
    public void onBindViewHolder(EnterprisesHolder holder, int position) {

        if(this.enterprises.get(position).getPhoto() == null){
            Glide.with(context).clear(holder.enterpriseImg);
            holder.enterpriseImg.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.img_e_1_lista));
        }
        else{
            Glide.with(context).load(this.enterprises.get(position).getPhoto()).into(holder.enterpriseImg);
        }


        holder.enterpriseName.setText(this.enterprises.get(position).getEnterprise_name());
        holder.enterpriseType.setText(this.enterprises.get(position).getEnterprise_type().getEnterprise_type_name());
        holder.enterpriseCountry.setText(this.enterprises.get(position).getCountry());


    }

    @Override
    public int getItemCount() {
        return this.enterprises.size();
    }


    public void OnItemClickListener(OnItemClickListener o){
        onItemClickListener = o;
    }
    public void openEnterpriseInfoActivity(int position){
        Enterprise enterprise = this.enterprises.get(position);
        Intent intent = new Intent(context, EnterpriseInfoActivity.class);
        intent.putExtra("Enterprise",enterprise);
        context.startActivity(intent);
    }

    public static class EnterprisesHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        ImageView enterpriseImg;
        TextView enterpriseName;
        TextView enterpriseType;
        TextView enterpriseCountry;


        public EnterprisesHolder(View itemView){
            super(itemView);
            this.enterpriseImg = (ImageView)itemView.findViewById(R.id.enterpriseImg);
            this.enterpriseName = (TextView)itemView.findViewById(R.id.enterpriseName);
            this.enterpriseType = (TextView)itemView.findViewById(R.id.enterpriseType);
            this.enterpriseCountry = (TextView)itemView.findViewById(R.id.enterpriseCountry);

            itemView.setOnClickListener(this);

        }


        @Override
        public void onClick(View v) {
            onItemClickListener.OnItemClickListener(v,getPosition());
        }


    }
}
