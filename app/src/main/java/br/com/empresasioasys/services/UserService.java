package br.com.empresasioasys.services;

import br.com.empresasioasys.models.User;
import br.com.empresasioasys.responses.LoginResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface UserService {


    @POST("users/auth/sign_in")
    Call<LoginResponse> loginUser(@Body User user);


}
