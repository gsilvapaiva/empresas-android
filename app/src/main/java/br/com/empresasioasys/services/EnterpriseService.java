package br.com.empresasioasys.services;


import br.com.empresasioasys.responses.EnterpriseListResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;


public interface EnterpriseService {


    @GET("enterprises")
    Call<EnterpriseListResponse> enterprisesList(@Header("access-token") String accessToken, @Header("client") String client, @Header("uid") String uid, @Query("enterprise_types") int enterprise_types,@Query("name") String name);

}
